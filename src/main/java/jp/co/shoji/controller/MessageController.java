package jp.co.shoji.controller;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.shoji.form.MessageForm;
import jp.co.shoji.service.MessageService;
@Controller
public class MessageController {

	@Autowired
    private MessageService messageService;

	@RequestMapping(value = "/message", method = RequestMethod.GET)
	public String message(Model model) {
	    MessageForm form = new MessageForm();
	    model.addAttribute("messageForm", form);
	    model.addAttribute("aMessage", "新規投稿");
	    return "message";
	}

    @RequestMapping(value = "/message", method = RequestMethod.POST)
    public String message(@Valid @ModelAttribute MessageForm form, BindingResult result, Model model) {
    	if(result.hasErrors()) {
            return "message";
    	} else {
	    	messageService.insertMessage(form.getText());
	        return "redirect:/top";
    	}
    }
}