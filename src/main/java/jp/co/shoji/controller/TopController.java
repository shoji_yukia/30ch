package jp.co.shoji.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.shoji.dto.CommentDto;
import jp.co.shoji.dto.MessageDto;
import jp.co.shoji.form.CommentForm;
import jp.co.shoji.form.DeleteCommentForm;
import jp.co.shoji.form.DeleteMessageForm;
import jp.co.shoji.service.TopService;


@Controller
public class TopController {
	@Autowired
	private TopService topService;

	@RequestMapping(value = "/top", method = RequestMethod.GET)
	public String top(Model model) {
		List<MessageDto> tempMessages = topService.getMessageAll();
		List<CommentDto> comments = topService.getCommentAll();
		List<MessageDto> messages = getMessages(tempMessages);
		model.addAttribute("aMessage", "20ch");
		model.addAttribute("messages", messages);
		model.addAttribute("comments", comments);
		return "top";
	}

	@RequestMapping(value = "/top", params = "comment", method = RequestMethod.POST)
	public String comment(@Valid @ModelAttribute CommentForm form, BindingResult result, Model model) {
		if (result.hasErrors()) {
			List<MessageDto> messages = topService.getMessageAll();
			List<CommentDto> comments = topService.getCommentAll();
			model.addAttribute("aMessage", "20ch");
			model.addAttribute("messages", messages);
			model.addAttribute("comments", comments);
			model.addAttribute("title", "エラー");
			// model.addAttribute("message", "以下のエラーを解消してください");
			return "top";
		} else {
			topService.insertComment(form);
			model.addAttribute("commentForm", form);
			return "redirect:/top";
		}
	}

	@RequestMapping(value = "/top", params = "deleteMessage", method = RequestMethod.POST)
	public String deleteMessage(@ModelAttribute DeleteMessageForm form, Model model) {
		topService.deleteMessage(form.getId());
		return "redirect:/top";
	}

	@RequestMapping(value = "/top", params = "deleteComment", method = RequestMethod.POST)
	public String deleteComment(@ModelAttribute DeleteCommentForm form, Model model) {
		topService.deleteComment(form.getId());
		return "redirect:/top";
	}

	private List<MessageDto> getMessages(List<MessageDto> list) {
		for(int i = 0; i < list.size() - 1; i++) {
			for(int j = 0; j < list.size() - 1; j++) {
				MessageDto dto1 = new MessageDto();
				MessageDto dto2 = new MessageDto();
				BeanUtils.copyProperties(list.get(j), dto1);
				BeanUtils.copyProperties(list.get(j + 1), dto2);
				if(dto1.getUpdatedDate().before(dto2.getUpdatedDate()) ) {
				    list.set(j, dto2);
				    list.set(j + 1, dto1);
				}/* else {
					list.set(j, dto1);
					list.set(j + 1, dto2);
				}*/
			}
		}
		return list;
	}
}