package jp.co.shoji.dto;

import java.util.Date;

public class MessageDto {
    private int id;
    private String text;
    private Date created_date;
    private int is_deleted;
    private Date updated_date;


    public int getId() {
    	return id;
    }
    public void setId(int id) {
    	this.id = id;
    }

    public String getText() {
    	return text;
    }
    public void setText(String text) {
    	this.text = text;
    }

    public Date getCreatedDate() {
    	return created_date;
    }
    public void setCreatedDate(Date created_date) {
    	this.created_date = created_date;
    }

    public int getIsDeleted() {
    	return is_deleted;
    }
    public void setIsDeleted(int is_deleted) {
    	this.is_deleted = is_deleted;
    }

    public Date getUpdatedDate() {
    	return updated_date;
    }
    public void setUpdatedDate(Date updated_date) {
    	this.updated_date = updated_date;
    }
}