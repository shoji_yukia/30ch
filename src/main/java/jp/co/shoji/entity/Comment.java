package jp.co.shoji.entity;

import java.util.Date;

public class Comment {
    private int id;
    private String text;
    private Date created_date;
    private int message_id;
    private int is_deleted;


    public int getId() {
    	return id;
    }
    public void setId(int id) {
    	this.id = id;
    }

    public String getText() {
    	return text;
    }
    public void setText(String text) {
    	this.text = text;
    }

    public Date getCreatedDate() {
    	return created_date;
    }
    public void setCreatedDate(Date created_date) {
    	this.created_date = created_date;
    }

    public int getMessageId() {
    	return message_id;
    }
    public void setMessageId(int message_id) {
    	this.message_id = message_id;
    }

    public int getIsDeleted() {
    	return is_deleted;
    }
    public void setIsDeleted(int is_deleted) {
    	this.is_deleted = is_deleted;
    }


}