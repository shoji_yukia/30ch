package jp.co.shoji.form;

import java.util.Date;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

public class CommentForm {

	private int id;
	@NotBlank(message = "文字を入力してください")
	@Size(message = "140字以内で入力してください", max = 140)
	private String text;
	private int message_id;
	private Date updated_date;


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getMessageId() {
		return message_id;
	}

	public void setMessageId(int message_id) {
		this.message_id = message_id;
	}

	public Date getUpdatedDate() {
    	return updated_date;
    }
    public void setUpdatedDate(Date updated_date) {
    	this.updated_date = updated_date;
    }
}