package jp.co.shoji.form;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

public class MessageForm {

	private int id;
	@NotBlank(message = "文字を入力してください")
	@Size(message = "140字以内で入力してください", max = 140)
	private String text;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}
