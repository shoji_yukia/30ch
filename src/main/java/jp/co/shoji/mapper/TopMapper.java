package jp.co.shoji.mapper;

import java.util.List;

import jp.co.shoji.entity.Comment;
import jp.co.shoji.entity.Message;
import jp.co.shoji.form.CommentForm;

public interface TopMapper {
    List<Message> getMessageAll();
    List<Comment> getCommentAll();
    void insertComment(CommentForm form);
    void deleteMessage(int id);
    void deleteComment(int id);
	void updateMessageDate(CommentForm form);
}