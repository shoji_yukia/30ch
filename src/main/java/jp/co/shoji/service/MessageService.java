package jp.co.shoji.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.shoji.mapper.MessageMapper;

@Service
public class MessageService {

    @Autowired
    private MessageMapper messageMapper;

    public void insertMessage(String text) {
        messageMapper.insertMessage(text);
        return;
    }

}