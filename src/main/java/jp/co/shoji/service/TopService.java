package jp.co.shoji.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.shoji.dto.CommentDto;
import jp.co.shoji.dto.MessageDto;
import jp.co.shoji.entity.Comment;
import jp.co.shoji.entity.Message;
import jp.co.shoji.form.CommentForm;
import jp.co.shoji.mapper.TopMapper;

@Service
public class TopService {

    @Autowired
    private TopMapper topMapper;

    public List<MessageDto> getMessageAll() {
        List<Message> messageList = topMapper.getMessageAll();
        List<MessageDto> resultList = convertToMessageDto(messageList);
        return resultList;
    }

    private List<MessageDto> convertToMessageDto(List<Message> messageList) {
        List<MessageDto> resultList = new LinkedList<MessageDto>();
        for (Message entity : messageList) {
            MessageDto dto = new MessageDto();
            BeanUtils.copyProperties(entity, dto);
            resultList.add(dto);
        }
        return resultList;
    }

    public List<CommentDto> getCommentAll() {
        List<Comment> commentList = topMapper.getCommentAll();
        List<CommentDto> resultList = convertToCommentDto(commentList);
        return resultList;
    }

    private List<CommentDto> convertToCommentDto(List<Comment> commentList) {
        List<CommentDto> resultList = new LinkedList<CommentDto>();
        for (Comment entity : commentList) {
            CommentDto dto = new CommentDto();
            BeanUtils.copyProperties(entity, dto);
            resultList.add(dto);
        }
        return resultList;
    }

    public void insertComment(CommentForm form) {
        topMapper.insertComment(form);
        topMapper.updateMessageDate(form);
        return;
    }

    public void deleteMessage(int id) {
    	topMapper.deleteMessage(id);
    	return;
    }

    public void deleteComment(int id) {
    	topMapper.deleteComment(id);
    	return;
    }

}