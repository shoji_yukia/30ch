<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
    <head>
        <meta charset="utf-8">
        <title>Welcome</title>
        <script type="text/javascript">
		    function checkMessage(){
			    var message = document.getElementById('messageText').value;
			    if(message.length > 140){
			        window.alert('140字以下で文字を入力してください');
			        return false;
			    }
			    if(!message || !message.match(/\S/g)){
			    	window.alert('文字を入力してください');
			        return false;
			    }
			    return true;
			}
		</script>
    </head>
    <body>
        <div class="header">
            <ul>
                 <li><a href="top">トップページ</a></li>
            </ul>
        </div>
        <h1>${aMessage}</h1>
        <h2><c:out value="${title}"></c:out></h2>
        <h2><c:out value="${message}"></c:out></h2>
        <form:form modelAttribute="messageForm">
            <div><form:errors path="*" /></div>
            <textarea name="text" id="messageText" cols="60" rows="5" class="text-box"></textarea>
            <input type="submit" value="投稿" onclick="return checkMessage()">
        </form:form>
    </body>
</html>