<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
    <head>
        <meta charset="utf-8">
        <title>20ch</title>
        <link rel="stylesheet" type="text/css" href="style.css">

		<script type="text/javascript">
		    function checkComment(num){
			    var comment = document.commentForm[num].text.value;
			    // 140文文字以下判定
			    if(comment.length > 140){
			        window.alert('140字以下で文字を入力してください');
			        return false;
			    }
			    // 空文字判定
			    else if(!comment || !comment.match(/\S/g)){
			    	window.alert('文字を入力してください');
			        return false;
			    }
			    return true;
			}
			function checkDelete(){
				// 入力ダイアログを表示 ＋ 入力内容を password に代入
				const password = window.prompt("管理者パスワードを入力してください", "");
					//
				if(password == 'admini'){
					// 削除処理
					window.alert('削除しました');
					return true;
				}
				// 入力内容が一致しない場合は警告ダイアログを表示
				else if(password != "" && password != null){
					window.alert('管理者パスワードが間違っています');
					return false;
				}
				// 空の場合やキャンセルした場合は警告ダイアログを表示
				else{
					window.alert('キャンセルされました');
					return false;
				}
			}
		</script>
    </head>
    <body>
        <div class="header">
            <ul>
                <li><a href="message">新規投稿</a></li>
            </ul>
        </div>
        <h1>${aMessage}</h1>
        <form:form modelAttribute="commentForm">
            <div><form:errors path="*" /></div><br />
        </form:form>
        <c:forEach items="${messages}" var="message" varStatus="status">
            <c:if test="${message.isDeleted == 0}">
	            <h2><pre><c:out value="${message.text}"></c:out></pre></h2>
	            <form:form modelAttribute="deleteMessageForm">
	                <input type="hidden" value="${message.id}" name="id" >
	                <input type="submit" value="削除" name="deleteMessage" onclick="return checkDelete()"><br />
	            </form:form>
	        </c:if>
	        <c:if test="${message.isDeleted == 1}">
	            <h4>この投稿は削除されました</h4>
	        </c:if>
            <c:forEach items="${comments}" var="comment">
	            <c:if test="${comment.messageId == message.id}">
	                <c:if test="${comment.isDeleted == 0}">
		                <h4><pre><c:out value="${comment.text}"></c:out></pre></h4>
		                <form:form modelAttribute="deleteCommentForm">
		                    <input type="hidden" value="${comment.id}" name="id" >
		                    <input type="submit" value="削除" name="deleteComment" onclick="return checkDelete()"><br />
		                </form:form>
		            </c:if>
		            <c:if test="${comment.isDeleted == 1}">
		                <h4>このコメントは削除されました</h4>
		            </c:if>
	            </c:if>
            </c:forEach>
            <form:form modelAttribute="commentForm" name="commentForm">
	            <textarea name="text" id="commentText" cols="60" rows="5" class="text-box"></textarea>
	            <input type="submit" value="コメント" name="comment" onclick="return checkComment(${status.index})">
	            <input type="hidden" value="${message.id}" name="messageId">
	        </form:form>
        </c:forEach>
    </body>
</html>